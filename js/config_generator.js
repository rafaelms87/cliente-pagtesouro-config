class Configuracao {

    constructor() {
        this.nomeOm = ""
        this.siglaOm = ""
        this.ambiente = "Produção"
        this.ugs = [

        ]
    }

}

class Ug {

    constructor() {
        this.codigo = ""
        this.token = ""
        this.servicos = []
    }

}

class Servico {

    constructor() {
        this.codigo = ""
        this.descricao = ""
    }

}

var config = new Configuracao()
var botaoDownload = $("#botao-download")
botaoDownload.click(download)
var tituloPendencias = $("#titulo-pendencias")
var pendencias = $("#pendencias")
var ugs = []
var currentUg = "";

$('#codigoNovaUg').mask('Z', { translation: { 'Z': { pattern: /[0-9]/, recursive: true } } });
$('#codigoNovoServico').mask('Z', { translation: { 'Z': { pattern: /[0-9]/, recursive: true } } });
$("#btnAdicionaUg").click(addUg)
$("#btnAdicionaServico").click(addServico)


$(".input-to-clone").keyup(r => {
    campo = r.target.dataset.link
    config[campo] = r.target.value
    updateChanges()
})

$(".select-to-clone").change(r => {
    campo = r.target.dataset.link
    config[campo] = r.target.value
    updateChanges()
})


$('#load').click(() => {

    var file = document.getElementById('configfile').files[0];
    var reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = function (evt) {
        config = JSON.parse(evt.target.result);
        updateChanges()
    }

})



function addUg() {
    let codigo = $("#codigoNovaUg").val()
    let token = $("#tokenNovaUg").val()

    if (codigo == "" || token == "" || token == undefined || token == undefined) {
        alert('Preencha todos os campos')
        return
    }

    let ug = new Ug()
    ug.codigo = codigo
    ug.token = token

    config.ugs.push(ug)
    $("#codigoNovaUg").val('')
    $("#tokenNovaUg").val('')
    $("#modalUg").modal('hide')
    updateChanges()
}

function setaUgServico(codigo) {
    currentUg = codigo;
    console.log("currentUg: "+currentUg)
}

function addServico() {
    let codigo = $("#codigoNovoServico").val()
    let descricao = $("#descricaoNovoServico").val()

    if (codigo == "" || descricao == "" || descricao == undefined || descricao == undefined) {
        alert('Preencha todos os campos')
        return
    }

    let servico = new Servico()
    servico.codigo = codigo
    servico.descricao = descricao

    for (let index = 0; index < config.ugs.length; index++) {
        let ugAtual = config.ugs[index]
        if (ugAtual.codigo == currentUg) {
            ugAtual.servicos.push(servico)
        }
    }


    updateChanges()
}


function removeUg(codigo) {
    for (let index = 0; index < config.ugs.length; index++) {
        let ugAtual = config.ugs[index]
        if (ugAtual.codigo == codigo) {
            config.ugs.splice(index, 1)
        }
    }
    document.querySelector(`#ug-${codigo}`).remove()
    updateChanges()
}

function removeServico(codigo) {
    for (let indexUg = 0; indexUg < config.ugs.length; indexUg++) {
        let ugAtual = config.ugs[indexUg]
        for (let indexServico = 0; indexServico < ugAtual.servicos.length; indexServico++) {
            let servicoAtual = ugAtual.servicos[indexServico]
            if (servicoAtual.codigo == codigo) {
                ugAtual.servicos.splice(indexServico, 1)
            }
        }

    }
    document.querySelector(`#servico-${codigo}`).remove()
    updateChanges()
}

function updateChanges() {

    $("#nomeOrganizacao").val(config.nomeOm)
    $("#siglaOrganizacao").val(config.siglaOm)
    let tudoOK = true
    let contador = 0
    pendencias.empty()
    if (config.nomeOm == "" || config.nomeOm == undefined) {
        tudoOK = false
        contador++
        pendencias.append(`<li>Nome da Organização não informado</li>`)
    }
    if (config.siglaOm == "" || config.siglaOm == undefined) {
        tudoOK = false
        contador++
        pendencias.append(`<li>Sigla da Organização não informado</li>`)
    }
    if (config.ugs == "" || config.nomeOm == undefined) {
        tudoOK = false
        contador++
        pendencias.append(`<li>Nenhuma UG Cadastrada</li>`)
    } else {
        config.ugs.forEach(ug => {
            if (ug.servicos.length < 1) {
                pendencias.append(`<li>UG ${ug.codigo} sem Serviços cadastrados</li>`)
                tudoOK = false
                contador++
            }
        });
    }

    if (tudoOK) {
        tituloPendencias.text("Sem pendências")
        botaoDownload.addClass('btn-success');
        botaoDownload.removeClass('btn-danger');
        botaoDownload.prop("disabled", false);
    } else {
        tituloPendencias.text(`${contador} pendências encontradas`)
        botaoDownload.removeClass('btn-success');
        botaoDownload.addClass('btn-danger');
        botaoDownload.prop("disabled", true);
    }

    $(".ugs").html("")

    config.ugs.forEach(ug => appendDivUg(ug))

}


function download() {
    var link = document.createElement('a');
    json = JSON.stringify(config)
    link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(json));
    link.setAttribute('download', "config.json");
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


function appendDivUg(ug) {

    $(".ugs").append(`
    <li class="item-ug" data-ug="${ug.codigo}" id="ug-${ug.codigo}">
    <button class="btn btn-danger btn-sm botao-delete rounded-circle" onClick="removeUg('${ug.codigo}')">X</button>
    <h5>Código</h5>
    <p class="ml-4 codigo">
        ${ug.codigo}
    </p>
    <h5>Token</h5>
    <p class="ml-4 token">
    ${ug.token}
    </p>
    <h5>Serviços <button type="button" class="float-right mb-2 btn btn-sm btn-primary" data-toggle="modal" data-target="#modalServico" onclick="setaUgServico(${ug.codigo})">Adicionar Serviço</button></h5>
    
    <table class="table table-hover table-stripped servicos">
    <thead>
        <tr>
            <th>Código</th>
            <th>Descrição</th>
            <th>Remover</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    </table>
</li>
    `)

    ug.servicos.forEach(sv => appendDivSv(ug.codigo, sv))

}

function appendDivSv(codug, servico) {


    $(`#ug-${codug} .servicos tbody`).append(`
    <tr class="item-servico" data-servico="${servico.codigo}" id="servico-${servico.codigo}">
    
    <td>
        ${servico.codigo}
    </td>
    <td>
    ${servico.descricao}
    </td>
    <td>
    <button class="btn btn-danger btn-sm" onClick="removeServico('${servico.codigo}')">remover</button>
    </td>
    
    <tr>
    `);
    currentUg = ""
    $("#codigoNovoServico").val('')
    $("#descricaoNovoServico").val('')
    $("#modalServico").modal('hide')

}

updateChanges()